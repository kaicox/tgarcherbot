#!/usr/bin/python3

from time import time
from pyrogram import Client, MessageHandler, Filters, ChatPermissions

app = Client("tgarcher", config_file=".config.ini") # bot_token="960412122:AAE5rL8RNvW8pSR0e7wK2s6NgbJeMfc_oyI")

trusted_bots_id = [175844556]
TARGET = ["-1001490988533", "-1001394610901"]
users = []

@app.on_message(Filters.chat(TARGET), group = 0)
def main(app, message):
        return 1

@app.on_message(Filters.chat(TARGET) and Filters.new_chat_members, group = 1)
def remove_robots(app, message):
        for new_user in message.new_chat_members:
                if(new_user.is_bot is True):
                        app.kick_chat_member(chat_id=message.chat.id, user_id=new_user.id)
                else:
                        uflag = 0
                        for u in users:
                                if(new_user.id == u): 
                                        uflag = 1
                        if(uflag == 0):
                                app.restrict_chat_member(message.chat.id, new_user.id, ChatPermissions(can_send_messages = 1), int(time() + 120))
                                users.append(new_user.id)

@app.on_message(Filters.chat(TARGET) and Filters.bot, group = 2)
def remove_messages(app, message):
        for bot_id in trusted_bots_id:
                if(bot_id != message.from_user.id):
                        app.delete_messages(chat_id=message.chat.id,message_ids=message.message_id)

@app.on_message(Filters.chat(TARGET) and Filters.service, group = 3)
def remove_services(app, message):
        app.delete_messages(chat_id=message.chat.id,message_ids=message.message_id)



app.add_handler(MessageHandler(main))
app.run()
